console.log("addCourse.js is linked");
const addCourseForm = document.querySelector("#createCourse");

const baseUrl = "https://shielded-caverns-78086.herokuapp.com/"

addCourseForm.addEventListener("submit", (e) => {
	e.preventDefault();
	
	let name = document.querySelector("#courseName").value;
	let description = document.querySelector("#courseDescription").value;
	let price = document.querySelector("#coursePrice").value;

	if (name.length !== 0 && description.length !== 0 && price.length !== 0) {
		addCourse(name, description, price);
	} else {
		errorAlert("Please fill out the form completely.");
	}
});

const addCourse = (name, description, price) => {
	fetch(baseUrl + "api/courses/courseExists", {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({
			name: name.trim(),
		})
	}).then(res => {
		return res.json()
	}).then(courseExists => {
		if (courseExists === true) {
			errorAlert("Course already exists.");
		} else {
			fetch(baseUrl + "api/courses/addCourse", {
				method: 'POST', 
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
				name: name.trim(),
				description: description,
				price: price
				})
			}).then(res => {
				return res.json()
			}).then(data => {
				console.log(data)
				if (data === true) {
					let courseName = name.trim();
					Swal.fire({
						icon: 'success',
						title: 'Course added!',
						text: `${courseName} is now in the course list.`,
						footer: '<a href="./courses.html">View Courses</a>'
					});
				} else {
					errorAlert("Please try again.")
				}
			});	
		}
	});
}

const errorAlert = (message) => {
	Swal.fire({
		icon: 'error',
		title: 'Something went wrong...',
		text: message,
		footer: '<a href="./courses.html">Return to Course List</a>'
	});
}