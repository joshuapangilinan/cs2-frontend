console.log("hello from JS")

const baseUrl = "https://shielded-caverns-78086.herokuapp.com/"
let token = localStorage.getItem("token")
console.log(token)

let profile = document.querySelector('#profileContainer')

if (!token || token === null) {
	alert("You must login first.");
	window.location.href="./login.html"
} else {
	fetch(baseUrl + 'api/users/details', {
		method: 'GET',
		headers: {
			'Content-type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json()).then(data => {
		console.log(data)
		let enrollmentData = data.enrollments;
		profile.innerHTML = `
			<div class="col-md-12">
				<section class="jumbotron my-5" id="courses-jumbotron">
					<h3 class="text-center">First Name: ${data.firstName}</h3>
					<h3 class="text-center">Last Name: ${data.lastName}</h3>
					<table class="table" id="courses-jumbotron">
					<h3 class="text-center">Email: ${data.email}</h3>
						<thead>
							<tr>
								<th>Course ID: ${enrollmentData}</th>
								<th>Enrolled on:</th>
								<th>Status:</th>
								<tbody></tbody>
							</tr>
						</thead>
					</table>
				</section>
			</div>
		`
	})
}