console.log("hello fromn JS"); 

let loginForm = document.querySelector('#loginUser')
const baseUrl = "https://shielded-caverns-78086.herokuapp.com/"
//const baseUrl = "http://localhost:4000/"

loginForm.addEventListener("submit", (e) => {
   e.preventDefault()

   let email = document.querySelector("#userEmail").value
   //console.log(email)
   let password = document.querySelector("#password").value 
   //console.log(password) 

   //how can we inform a user that a blank input field cannot be logged in?
   if (email == "" || password == "") {
      errorAlert("Please enter a username and/or password.")
   } else {
      fetch(baseUrl + 'api/users/login', {
      	method: 'POST',
      	headers: {
      		'Content-Type': 'application/json'
      	},
      	body: JSON.stringify({
      		email: email,
      		password: password
      	})
      }).then(res => {
      	return res.json()
      }).then(data => {
      	console.log(data.access)
      	
      	if(data.access){
           localStorage.setItem('token', data.access)
           console.log(data);
           // alert("access key saved on local storage.")
           fetch(baseUrl + 'api/users/details', {
           	headers: {
           		'Authorization': `Bearer ${data.access}`
           	}
           }).then(res => {
           	  return res.json()
           }).then(data => {
           	  localStorage.setItem("id", data._id);
              localStorage.setItem("name", `${data.firstName}`); 
           	  localStorage.setItem("isAdmin", data.isAdmin);
           	  console.log("items are saved to local storage.")

              Swal.fire({
                icon: 'success',
                title: 'Logging in...'
              })
              window.location.replace('./courses.html')
              console.log(data);
           })
      	} else {
           errorAlert("Invalid username and/or password.")
      	}
      })
   }
})

const errorAlert = (message) => {
  Swal.fire({
    icon: 'error',
    title: 'Something went wrong...',
    text: message,
    footer: '<a href="./register.html">Register a New User</a>'
  });
}