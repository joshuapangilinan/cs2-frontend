console.log("hello from script.js")
let navItem = document.querySelector('#navSession') 
let isIndex = document.querySelector('#isIndex') !== null

let userToken = localStorage.getItem("token")
console.log(userToken)
console.log("is Index?", isIndex)

if (!userToken) {
  if (isIndex) {
    navItem.innerHTML =
   `
    <li class="nav-item">
      <a class="btn btn-primary" href="./pages/login.html" class="nav-link">Log In</a>
    </li>
   `
  } else {
     navItem.innerHTML =
   `
    <li class="nav-item">
      <a class="btn btn-primary" href="./login.html" class="nav-link">Log In</a>
    </li>
   `
  }
} else {
  if (isIndex) {
       navItem.innerHTML =
   `
   <li class="nav-item">
        <a class="btn btn-danger" href="./pages/logout.html" class="nav-link">Log Out</a>
   </li>
   `
  } else {
      navItem.innerHTML =
   `
   <li class="nav-item">
        <a class="btn btn-danger" href="./logout.html" class="nav-link">Log Out</a>
   </li>
   `
  }
}