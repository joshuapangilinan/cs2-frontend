console.log("henlo")

let modalButton = document.querySelector('#adminButton');
let container = document.querySelector('#coursesContainer');
let isAdmin = localStorage.getItem("isAdmin");
let name = localStorage.getItem("name");
let cardFooter;

const baseUrl = "https://shielded-caverns-78086.herokuapp.com/"
//const baseUrl = "http://localhost:4000/"

console.log("isAdmin", isAdmin)

if (isAdmin === null) {
	modalButton.innerHTML = null;
} else if (isAdmin === "false" || !isAdmin) {
	modalButton.innerHTML = `<div class="welcome-text"><h2>Welcome, ${name}.</h2><p>These are the available courses.</p></div>`
	//modalButton.innerHTML = null;
} else {
	modalButton.innerHTML = `
	<div class="welcome-text"><h2>Welcome, ${name}.</h2><p>You have administrator privileges.</p></div>
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
      	</div>
	`
}

fetch(baseUrl + 'api/courses/').then(res => res.json().then(data => {
	console.log(data);
	let courseData;
	if (data.length < 1) {
		courseData = "No course available";
	} else {
		courseData = data.map(course => {
			console.log(course._id);

			if (isAdmin == "false" || !isAdmin) {
				cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View Course Details</a>
				
				`
			} else {
				cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Edit</a>
				<a href="./course.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">Disable Course</a>
				`
			}

			return (
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body card-med">
							<h5 class="card-title">${course.name}</h5>
							<p class="card-text text-left">${course.description}</p>
							<p class="card-text text-left">${course.price}</p>
							<p class="card-text text-left">${course.createdOn}</p>
						</div>
						<div class="card-footer card-footer-med">
							${cardFooter}
						</div>
					</div>
				</div>
				`	
			);
		}).join("");
	}
	container.innerHTML = courseData;
}));