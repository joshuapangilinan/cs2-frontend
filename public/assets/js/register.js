console.log("register.js is linked");

const baseUrl = "https://shielded-caverns-78086.herokuapp.com/"
let registerUser = document.querySelector("#registerUser");

registerUser.addEventListener("submit", (e) => {
	e.preventDefault();
	
	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let email = document.querySelector("#userEmail").value;
	let mobile = document.querySelector("#mobileNumber").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	//if (password1.length + password2.length == 0 || mobile.length !== 11) {
	if (firstName.length + lastName.length == 0) {
		errorAlert('Please fill out the form completely.');
	} else if (email.length == 0) {
		errorAlert('Please enter an email address.');
	} else if (mobile.length == 0) {
		errorAlert('Please enter a valid mobile number.');
	} else if (password1.length + password2.length == 0) {
		errorAlert('Please enter a password');
	} else if (password1 !== password2) {
		errorAlert('Passwords must match.')
	} else {
		// check if email already exists
		fetch(baseUrl + 'api/users/email-exists', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email
			})
		}).then(res => res.json()).then(data => {
			if (data === false) {
				register(firstName, lastName, email, mobile, password1);
			} else {
				errorAlert("That email address is already in use")
			}
		});
	}
});

const register = (firstName, lastName, email, mobile, password) => {
	fetch(baseUrl + "api/users/register", {
		method: 'POST', 
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			email: email,
			mobileNo: mobile,
			password: password
		})
	}).then(res => {
		return res.json()
	}).then(data => {
		console.log(data)
		if (data === true) {
			Swal.fire({
			icon: 'success',
			title: 'User registered!',
			text: 'You may now log in.',
			footer: '<a href="./login.html>Continue to Log In</a>'
		})
			//window.location.replace('./login.html')
		} else {
			errorAlert("Please try again.")
		}
	});	
}

const errorAlert = (message) => {
	Swal.fire({
		icon: 'error',
		title: 'Something went wrong...',
		text: message
	});
}