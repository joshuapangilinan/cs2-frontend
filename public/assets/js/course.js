//import { hi } from './constants.js';

console.log("Hello from course.js");

const baseUrl = "https://shielded-caverns-78086.herokuapp.com/"
//const baseUrl = "http://localhost:4000/"

// use course ID to identify which course to display
// window.location -> current location of document
// .search -> query string section of url
let params = new URLSearchParams(window.location.search);
let id = params.get('courseId');
console.log(id);

// capture access token
let token = localStorage.getItem('token');
console.log('Got token:', token);

// sections of html body
let name = document.querySelector("#courseName");
let description = document.querySelector("#courseDesc");
let price = document.querySelector("#coursePrice");
let enroll = document.querySelector("#enrollmentContainer");
let enrollButton;

fetch(`${baseUrl}api/courses/${id}`).then(res => {
	return res.json();
}).then(data => {
	//console.log(data);

	name.innerHTML = data.name;
	description.innerHTML = data.description;
	price.innerHTML = data.price;
	enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`
	//enrollButton = document.querySelector(#enrollButton);
});

enroll.addEventListener('click', (e) => {
	e.preventDefault();
	
	fetch(`${baseUrl}api/users/enroll`, {
		method: 'POST',
		headers: { 
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}` 
		},
		body: JSON.stringify({
			// userId: token,
			courseId: id 
		})
	}).then(res => {
		return res.json()
	}).then(data => {
		if (data === true) {
			Swal.fire('You are enrolled!')
		} else {
			Swal.fire('Enrollment failed.')
		}
	})
});